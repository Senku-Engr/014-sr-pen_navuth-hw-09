
let f = document.getElementById('form');
let name = document.getElementById("name");
let phone = document.getElementById("phone");
let spanName = document.getElementById("spanName");
let spanPhone = document.getElementById("spanPhone");
let btnSubmit = document.getElementById("btnSubmit");
let tbody = document.getElementById("tbodys");
let i = 1;
let myobj;

f.onsubmit = function (event) {
    event.preventDefault()
    if (btnSubmit.innerHTML == "Add") {
        validateName(name.value)
        validatePhone(phone.value)
        if (validateName(name.value) == false && validatePhone(phone.value) == false) {
            //get value from input to create object
            let person = {
                name: name.value,
                phone: phone.value,

            }
            //show to html table
            tbody.innerHTML +=
                `
                    <tr>
                    <td scope="col">${i}</td>
                    <td scope="col">${person.name}</td>
                    <td scope="col">${person.phone}</td>
                    <td scope="col">
                        <button class="btn btn-elegant btnsize" onclick="view(this)">View</button>
                        <button class="btn btn-danger btnsize" onclick="deletee(this)">Delete</button>
                        <button class="btn btn-info btnsize" onclick="edit(this)">Edit</button>
                    </td>
                </tr>
          
                `
            i++;
            name.value = ''
            phone.value = ''
        }
    } else {
        update(myobj);

    }

}
function update(obj) {

    validateName(name.value)
    validatePhone(phone.value)
    if (validateName(name.value) == false && validatePhone(phone.value) == false) {
        let person = {
            name: name.value,
            phone: phone.value,

        }
        var editchild = obj.parentNode.parentNode.childNodes;
        editchild[3].innerHTML = person.name;
        editchild[5].innerHTML = person.phone;
        btnSubmit.setAttribute("class", "btn btn-primary btn-block my-4 mysubmit")

        btnSubmit.innerHTML = 'Add';
        name.value = ''
        phone.value = ''

    }


}

function edit(obj) {
    btnSubmit.setAttribute("class", "btn btn-warning btn-block my-4 mysubmit")
    btnSubmit.innerHTML = 'Update';
    var editchild = obj.parentNode.parentNode.childNodes;
    name.value = editchild[3].innerHTML;
    phone.value = editchild[5].innerHTML;
    myobj = obj;

}


function view(obj) {
    var childs = obj.parentNode.parentNode.childNodes;
    alert("ID : " + childs[1].innerHTML + ", Name : " + childs[3].innerHTML + ", Phone : " + childs[5].innerHTML)
}

function deletee(obj) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
            var trchild = obj.parentNode.parentNode;
            trchild = trchild.parentNode.removeChild(trchild);

        }
    })
}


function validateName(name) {
    let patt = /[^a-z \s]/gi
    if (name == "") {
        spanName.innerHTML = `<i>* name blank<i>`
        spanName.style.color = 'red'
        spanName.style.fontSize = '15px'
        spanName.style.textAlign = 'left'
    } else if (name.match(patt) != null) {
        spanName.innerHTML = `<i>* name not allow any symbols<i>`
        spanName.style.color = 'red'
        spanName.style.fontSize = '15px'
        spanName.style.textAlign = 'left'
    } else {
        spanName.innerHTML = ''
        return false
    }
}
function validatePhone(phone) {
    let patt = /[^0-9]/gi
    if (phone == "") {
        spanPhone.innerHTML = `<i>* phone blank<i>`
        spanPhone.style.color = 'red'
        spanPhone.style.fontSize = '15px'
        spanPhone.style.textAlign = 'left'
    } else if (phone.match(patt) != null) {
        spanPhone.innerHTML = `<i>* phone not allow any symbols<i>`
        spanPhone.style.color = 'red'
        spanPhone.style.fontSize = '15px'
        spanPhone.style.textAlign = 'left'
    } else {
        if (phone.length >= 10) {
            spanPhone.innerHTML = `<i>* phone cannot more than 10 numbers<i>`
            spanPhone.style.color = 'red'
            spanPhone.style.fontSize = '15px'
            spanPhone.style.textAlign = 'left'
        } else {
            spanPhone.innerHTML = ''
            return false
        }
    }
}




